# Integrated Genome Browser Docker Image

This project contains dockerfile to build a docker image used to build Integrated Genome Browser using Bitbucket pipelines.

It contains:

* Oracle Java 1.8
* Maven 3.5.5
* Install4J 7.0

## Details

### Oracle Java 1.8

This image extends 'maven:3.5.0', which contains openjdk 1.8. However, IGB requires JavaFX, which is not included
in openjdk 1.8. So we replace this with [OracleJDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).

### Install4J

We use [Install4j](https://www.ej-technologies.com/download/install4j/files) to create multi-platform installers, which contain Java.

Note that using Install4J requires a license key. The docker image **does not** include the license key, as this is private.

## Links 

The Dockerfile uses the following links:

1. **Oracle JDK 1.8**: http://download.oracle.com/otn-pub/java/jdk/8u191-b12/2787e4a523244c269598db4e85c51e0c/jdk-8u191-linux-x64.tar.gz 
2. **Install4J 7.0**: https://download-keycdn.ej-technologies.com/install4j/install4j_linux_7_0_8.deb
3. **JRE Bundles** (Required by Install4J:
      1. Mac: https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/macosx-amd64-1.8.0_192_unpacked.tar.gz
      1. Windows 86x: https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/windows-x86-1.8.0_192.tar.gz
      1. Windows 64x: https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/windows-amd64-1.8.0_192.tar.gz
      1. Linux: https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/linux-amd64-1.8.0_191.tar.gz
      
      **Note**: These bundles are created by lorainelab.org. It is advisable to create your own jre bundle using install4j GUI application. For more info refer [Install4j Documentation](https://resources.ej-technologies.com/install4j/help/doc/help.pdf)
                JRE bundles can also be downloaded from [here](https://download.ej-technologies.com/bundles/list)
                As of Install4j 7.0.8, they have officially stopped support for linux bundles. You download it from oracle downloads page and create **.tar.gz** compression. 

## Future Maintenance (For IGB developers)

### Oracle JDK & Install4J:

* In case of version upgrade, you need to update the downloading links.
* Update the downloaded file names at places in Dockerfile where applicable.

eg. replace jdk-8u151-linux-x64.tar.gz with the new file name

```
wget --no-check-certificate -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u151-b12/e758a0de34e24606bca991d704f6dcbf/jdk-8u151-linux-x64.tar.gz && \
    #Installing Oracle JDK
    mkdir /usr/lib/jvm && \
    cp jdk-8u151-linux-x64.tar.gz /usr/lib/jvm/ && \
    tar xvzf jdk-8u151-linux-x64.tar.gz -C /usr/lib/jvm/ && \
    ln -sf usr/lib/jvm/jdk1.8.0_151 docker-java-home && \
    update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_151/bin/java" 1 && \
    update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_151/bin/javac" 1 && \
    update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/lib/jvm/jdk1.8.0_151/bin/javaws" 1 && \
```

## Developers:

* [Sanket Patil](mailto:spatil26@uncc.edu)
* [Ann Loraine](mailto:Ann.Loraine@uncc.edu)
* [Kiran Korey](mailto:kkorey@uncc.edu)